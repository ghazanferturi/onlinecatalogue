﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using OnlineCatalogue.DataTransferObjects;
using OnlineCatalogue.Entities;
using OnlineCatalogue.Helpers;
using OnlineCatalogue.Services;

namespace OnlineCatalogue.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : Controller
    {
        private ISubscriptionService _subscriptionService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public BookController(
            ISubscriptionService subscriptionService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _subscriptionService = subscriptionService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpGet]
        public IActionResult GetAllBooks()
        {
            var books = _subscriptionService.GetAllBooks();
            var booksDTO = _mapper.Map<IList<BookDTO>>(books);
            return Ok(booksDTO);
        }

        [HttpGet("{id}")]
        public IActionResult GetBookById(int id)
        {
            var book = _subscriptionService.GetBookById(id);
            var bookDTO = _mapper.Map<BookDTO>(book);
            return Ok(bookDTO);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _subscriptionService.Delete(id);
            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]BookDTO bookDTO)
        {
            var book = _mapper.Map<Book>(bookDTO);
            book.Id = id;
            try
            {
                _subscriptionService.Update(book);
                return Ok();
            } catch(AppException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Subscribe([FromBody]BookDTO bookDTO)
        {
            var book = _mapper.Map<Book>(bookDTO);
            try
            {
                _subscriptionService.Create(book, bookDTO.Name, bookDTO.Text, bookDTO.PurchasePrice);
                return Ok();
            } catch(AppException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
