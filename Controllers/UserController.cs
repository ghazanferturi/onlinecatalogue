﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using OnlineCatalogue.DataTransferObjects;
using OnlineCatalogue.Entities;
using OnlineCatalogue.Helpers;
using OnlineCatalogue.Services;

namespace OnlineCatalogue.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class UserController : Controller
    {
        private IRegistrationService _registrationService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public UserController(
            IRegistrationService registrationService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _registrationService = registrationService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _registrationService.GetAll();
            var usersDTO = _mapper.Map<IList<UserDTO>>(users);
            return Ok(usersDTO);
        }

        [HttpGet("{id}")]
        public IActionResult GetById([FromRoute] int id)
        {
            var user = _registrationService.GetById(id);
            var userDTO = _mapper.Map<UserDTO>(user);
            return Ok(userDTO);
        }

        /*
        [HttpGet("{email}")]
        public IActionResult GetByEmail(string email)
        {
            var user = _registrationService.GetByEmail(email);
            var userDTO = _mapper.Map<UserDTO>(user);
            return Ok(userDTO);
        }
        */

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _registrationService.Delete(id);
            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult Update([FromRoute] int id, [FromBody] UserDTO userDTO)
        {
            var user = _mapper.Map<User>(userDTO);
            user.Id = id;
            try
            {
                _registrationService.Update(user);
                return Ok();
            } catch(AppException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Register([FromBody] UserDTO userDTO)
        {
            var user = _mapper.Map<User>(userDTO);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _registrationService.Create(user);
            return Ok();
        }
    }
}
