﻿using Microsoft.EntityFrameworkCore;
using OnlineCatalogue.Entities;

namespace OnlineCatalogue.Helpers
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            /*
            modelBuilder.Entity<User>()
                .HasOne<Book>(pr => pr.BookSubscribed)
                .WithOne(bk => bk.SubscribedUser)
                .HasForeignKey<Book>(bk => bk.BookOfUserId);
            */
        }
        

        public DbSet<User> Users { get; set; }

        public DbSet<Book> Books { get; set; }
    }
}
