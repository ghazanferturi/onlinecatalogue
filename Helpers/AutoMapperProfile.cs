﻿using AutoMapper;
using OnlineCatalogue.DataTransferObjects;
using OnlineCatalogue.Entities;

namespace OnlineCatalogue.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();

            CreateMap<Book, BookDTO>();
            CreateMap<BookDTO, Book>();
        }
    }
}
