﻿using OnlineCatalogue.Entities;
using OnlineCatalogue.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace OnlineCatalogue.Services
{
    public interface ISubscriptionService
    {
        IEnumerable<Book> GetAllBooks();
        Book GetBookById(int id);
        Book Create(Book book, string name, string text, double purchaseprice);
        void Update(Book book);
        void Delete(int id);
    }

    public class SubscriptionService : ISubscriptionService
    {
        private DataContext _context;

        public SubscriptionService(DataContext context)
        {
            _context = context;
        }

        public Book Create(Book book, string name, string text, double purchaseprice)
        {
            if (string.IsNullOrEmpty(name))
                throw new AppException("Book Name is required");

            if (string.IsNullOrEmpty(text))
                throw new AppException("Book Text is required");

            if (double.IsNaN(purchaseprice) || double.IsNegative(purchaseprice))
                throw new AppException("Book Price must be positive float number");

            if (_context.Books.Any(x => x.Name == book.Name))
                throw new AppException("Book Name " + book.Name + " is already existed");

            book.Name = name;
            book.Text = text;
            book.PurchasePrice = purchaseprice;

            _context.Books.Add(book);
            _context.SaveChanges();

            return book;
        }

        public void Delete(int id)
        {
            var book = _context.Books.Find(id);
            if(book != null)
            {
                _context.Books.Remove(book);
                _context.SaveChanges();
            }
        }

        public IEnumerable<Book> GetAllBooks()
        {
            return _context.Books;
        }

        public Book GetBookById(int id)
        {
            return _context.Books.Find(id);
        }

        public void Update(Book book)
        {
            var bookContext = _context.Books.Find(book.Id);
            if (bookContext == null)
                throw new AppException("Book not found!");

            if(book.Name != bookContext.Name)
            {
                if (_context.Books.Any(x => x.Name == book.Name))
                    throw new AppException("Book Name " + book.Name + " already existed");
            }

            bookContext.Name = book.Name;
            bookContext.Text = book.Text;
            bookContext.PurchasePrice = book.PurchasePrice;

            _context.Books.Update(bookContext);
            _context.SaveChanges();
        }
    }
}
