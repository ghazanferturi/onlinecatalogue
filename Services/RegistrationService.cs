﻿using OnlineCatalogue.DataTransferObjects;
using OnlineCatalogue.Entities;
using OnlineCatalogue.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCatalogue.Services
{
    public interface IRegistrationService
    {
        Task Initialize(DataContext data);
        IEnumerable<User> GetAll();
        User GetById(int id);
        User GetByEmail(string email);
        User Create(User user);
        void Update(User user);
        void Delete(int id);
    }

    public class RegistrationService : IRegistrationService
    {
        private DataContext _context;

        public RegistrationService(DataContext context)
        {
            _context = context;
        }

        public async Task Initialize(DataContext data)
        {
            data.Users.Add(new User() { Id = 1, Email = "somemail@email.com", FirstName = "someone", LastName = "thatone" });
            data.Users.Add(new User() { Id = 2, Email = "someemail@email.com", FirstName = "som2eone", LastName = "tha2tone" });
            data.Users.Add(new User() { Id = 3, Email = "some2mail@email.com", FirstName = "some3one", LastName = "th1atone" });
            data.Users.Add(new User() { Id = 4, Email = "some3mail@email.com", FirstName = "some2one", LastName = "tha4tone" });

            await data.SaveChangesAsync();
        }
        
        public User Create(User user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();

            return user;
        }

        public void Delete(int id)
        {
            var user = _context.Users.Find(id);
            if(user != null)
            {
                _context.Users.Remove(user);
                _context.SaveChanges();
            }
        }

        public IEnumerable<User> GetAll()
        {
            return _context.Users;
        }

        public User GetByEmail(string email)
        {
            return _context.Users.Find(email);
        }

        public User GetById(int id)
        {
            return _context.Users.Find(id);
        }

        public void Update(User user)
        {
            var userContext = _context.Users.Find(user.Id);

            if (userContext == null)
                throw new Exception("User not found or Inactive");

            if(user.Email != userContext.Email)
            {
                if (_context.Users.Any(x => x.Email == user.Email))
                    throw new Exception("Email " + user.Email + " is already existed!");
            }

            /*
            userContext.FirstName = user.FirstName;
            userContext.LastName = user.LastName;
            userContext.Email = user.Email;
            userContext.IsActive = user.IsActive;
            */

            _context.Users.Update(userContext);
            _context.SaveChanges();
        }
    }
}
