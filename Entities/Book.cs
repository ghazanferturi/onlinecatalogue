﻿
namespace OnlineCatalogue.Entities
{
    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public double PurchasePrice { get; set; }
        public int BookOfUserId { get; set; }
        public User SubscribedUser { get; set; }
    }
}
