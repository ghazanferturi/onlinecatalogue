import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class RegisterService {
  private headers: HttpHeaders;
  private accessPointUrl: string = 'http://localhost:57295/api/register';

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8'});
  }

  public getAll() {
    return this.http.get(this.accessPointUrl, { headers: this.headers });
  }

  getById(id: number) {
    return this.http.get(this.accessPointUrl + '/users/' + id, {headers: this.headers});
  }

  create(payload) {
    return this.http.post(this.accessPointUrl, payload, {headers: this.headers});
  }

  update(user: User) {
    return this.http.put(this.accessPointUrl + '/users/' + user.id, user, {headers: this.headers});
  }

  delete(id: number) {
    return this.http.delete(this.accessPointUrl + '/users/' + id, {headers: this.headers});
  }

}

export class User {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
}
